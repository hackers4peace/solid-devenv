FROM ubuntu:16.04
MAINTAINER elf Pavlik <elf-pavlik@hackers4peace.net>
RUN apt-get update && apt-get install -y \
    curl \
    git \
    python \
    nginx \
    haproxy \
    build-essential
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -
RUN apt-get install -y nodejs
ENV PYTHON python2
RUN npm install -g pm2 solid-server


COPY ./switchtower /opt/switchtower
COPY ./config/switchtower/config.json /opt/switchtower/config.json
COPY ./config/switchtower/mydb /opt/switchtower/mydb
WORKDIR /opt/switchtower
RUN npm install --production

COPY ./cors-proxy /opt/cors-proxy
COPY ./config/cors-proxy/config.json /opt/cors-proxy/config.json
WORKDIR /opt/cors-proxy
RUN npm install --production

COPY ./storage /opt/storage
COPY ./idp /opt/idp
COPY ./nginx.conf /opt/
COPY ./haproxy.cfg /opt/
COPY ./run_services.sh /opt/

EXPOSE 80 443

WORKDIR /opt/
CMD ["/opt/run_services.sh"]
