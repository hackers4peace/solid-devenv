# solid-devenv

Development environment for developing solid web apps

## docker image

This image runs haproxy, nginx and 3 node servers (solid, permalinks and cors-proxy)

build it
``` shell
git submodule init
git submodule update
docker build -t solid .
```

run it
```shell
docker run --name solid -d -p 80:80 -p 443:443 solid
```

later start/stop it with
```shell
docker start solid
docker stop solid
```

## domains

```
# /etc/hosts
127.0.0.1	localhost idp.dev me.idp.dev perma.dev proxy.dev dataset.solid.dev blobs.solid.dev
```

## browser certificate

Import `me.idp.dev.p12` certificate to browser which you use for development. Since solid server uses self signed certificate, visit https://dataset.solid.dev/ and add security exception (this will prevent fetch requests in app to fail with `net::ERR_INSECURE_RESPONSE`.
