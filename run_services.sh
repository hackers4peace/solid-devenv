#!/bin/bash
cd /opt/storage && pm2 start solid -- start \
&& cd /opt/switchtower && pm2 start index.js \
&& cd /opt/cors-proxy && pm2 start index.js \
&& cd /opt && nginx -c /opt/nginx.conf \
&& haproxy -f /opt/haproxy.cfg
